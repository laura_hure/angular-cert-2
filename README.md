# Cheers

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 17.0.7.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.


<footer>
  <h1>Application permettant de gérer une liste de cocktails</h1>
  <p>Première page content un bandeau de filtres et une liste de card de cocktails</p>

  <h3>cocktails.service.ts</h3>
  <p>
    Permet de gérer les données des cocktails :  <br>
    - getAll() <br>
    - getById() <br>
    - update() <br>
  </p>

  <h3>list-cocktails.component.ts</h3>
  <p>
    Lien : /cocktails
    L'objet du module est de gérer une liste de cocktails. En faisant appel au composants partagés "filters" et "cards" <br>
    Le component devra gérer l'initialisation des filtres. <br>
    Et exécuter un custom pipe pour filtrer la liste de cocktails avant d'afficher les cards components <br>
    Les cards seront des templates qui feront appel à un petit component "toggle-stars"
  </p>


  <h3>detail-cocktail.component.ts</h3>
  <p>
    Lien :  /cocktails/:cocktailId <br>
    Page permettant d'afficher le détail d'un cocktails.
  </p>
  
  <h3>filters.component.ts</h3>
  <p>
    Le composant devrais être un formulaire dynamique de type array pour pouvoir y ajouter plus tard d'autres filtres. <br>
    Le component parent devra fournis le nom du libelle à afficher pour chaque filtres (servira de clé). <br>
    Le composant enfant devra ressortir une map de key/value avec pour key le libellé du filtre.
  </p>

  <h3>toggle-stars.component.ts</h3>
  <p> 
    Prend un état par défaut <br>
    Emet un event lors du click sur l'étoile "favoris" et retourne l'état de l'étoile
  </p>

  <h3>filter.pipe.ts</h3>
  <p>Prend une valeur et une clé et filtre une liste d'objets en fonction du couple clé/valeur</p>
</footer>
