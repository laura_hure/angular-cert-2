import { Routes } from '@angular/router';
import { ListeCocktailsComponent } from './liste-cocktails/liste-cocktails.component';
import { DetailsCocktailComponent } from './details-cocktail/details-cocktail.component';

export const routes: Routes = [
    { path: 'cocktails',  component: ListeCocktailsComponent,},
    { path: 'cocktails/:id', component: DetailsCocktailComponent},
    { path: '', redirectTo: 'cocktails', pathMatch: 'full' }
];

