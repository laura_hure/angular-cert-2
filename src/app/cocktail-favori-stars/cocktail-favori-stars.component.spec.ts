import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CocktailFavoriStarsComponent } from './cocktail-favori-stars.component';

describe('CocktailFavoriStarsComponent', () => {
  let component: CocktailFavoriStarsComponent;
  let fixture: ComponentFixture<CocktailFavoriStarsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CocktailFavoriStarsComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(CocktailFavoriStarsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
