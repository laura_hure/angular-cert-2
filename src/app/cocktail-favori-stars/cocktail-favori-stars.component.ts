import { Component, effect, input } from '@angular/core';
import { CocktailsService } from '../services/cocktails.service';

@Component({
  selector: 'app-cocktail-favori-stars',
  standalone: true,
  imports: [],
  templateUrl: './cocktail-favori-stars.component.html',
  styleUrl: './cocktail-favori-stars.component.scss'
})
export class CocktailFavoriStarsComponent {
  id = input.required<string>();

  

  constructor(private cocktailService: CocktailsService){}

   /**
   * Propage la modification du statut de favorie d'un cocktail
   * @param cocktailId 
   */
   toggleFavori(): void {
    this.cocktailService.toggleFavorie(this.id());
  }

  /**
   * Indique si le cocktaile est favoris ou non
   */
  get isFav(): boolean{
    return this.cocktailService.getCocktailFavById(this.id())
  }

}
