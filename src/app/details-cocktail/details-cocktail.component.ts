import { Component, Inject, Injector, input, Input, Signal, signal, TemplateRef } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CocktailsService } from '../services/cocktails.service';
import { Cocktail } from '../models/cocktail.model';
import { Observable, Subscription } from 'rxjs';
import {MatGridListModule} from '@angular/material/grid-list';
import { AsyncPipe, CommonModule, JsonPipe } from '@angular/common';
import {MatListModule} from '@angular/material/list';
import {MatChipsModule} from '@angular/material/chips';
import {MatIconModule} from '@angular/material/icon';
import { CocktailFavoriStarsComponent } from '../cocktail-favori-stars/cocktail-favori-stars.component';
import { toSignal } from '@angular/core/rxjs-interop'

@Component({
  selector: 'app-details-cocktail',
  standalone: true,
  imports: [
    CommonModule,
    MatGridListModule,
    JsonPipe,
    MatListModule,
    MatChipsModule,
    MatIconModule,
    RouterModule,
    CocktailFavoriStarsComponent,
    AsyncPipe
  ],
  templateUrl: './details-cocktail.component.html',
  styleUrl: './details-cocktail.component.scss'
})
export class DetailsCocktailComponent {
  cocktail$: Observable<Cocktail>;
  id = input.required<string>();

  constructor(private cocktailsService: CocktailsService) {}

  ngOnInit() {
    this.cocktail$ = this.cocktailsService.getById(this.id());
  }
}
