import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, output, Output, OutputRef } from '@angular/core';
import { outputFromObservable } from '@angular/core/rxjs-interop';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { of, Subscription } from 'rxjs';

@Component({
  selector: 'app-filters',
  standalone: true,
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule
  ],
  templateUrl: './filters.component.html',
  styleUrl: './filters.component.scss'
})
export class FiltersComponent {
  search: FormControl<string | null> = new FormControl('');

  @Input()
  label: string = '';

  onFilterValueChange: OutputRef<string | null>  = outputFromObservable<string | null>(this.search.valueChanges);
}
