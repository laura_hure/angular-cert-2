import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeCocktailsComponent } from './liste-cocktails.component';

describe('ListeCocktailsComponent', () => {
  let component: ListeCocktailsComponent;
  let fixture: ComponentFixture<ListeCocktailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ListeCocktailsComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ListeCocktailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
