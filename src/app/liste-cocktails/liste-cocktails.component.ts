import { Component } from '@angular/core';
import {MatCardModule} from '@angular/material/card';
import { CocktailsService } from '../services/cocktails.service';
import { AsyncPipe, CommonModule, JsonPipe } from '@angular/common';
import { FiltersComponent } from '../filters/filters.component';
import { FiltreObjetsPipe } from '../pipes/filtre-objets.pipe';
import { RouterModule } from '@angular/router';
import { CocktailFavoriStarsComponent } from '../cocktail-favori-stars/cocktail-favori-stars.component';
import { Observable } from 'rxjs';
import { Cocktail } from '../models/cocktail.model';

@Component({
  selector: 'app-liste-cocktails',
  standalone: true,
  imports: [  
    CommonModule,
    MatCardModule,
    JsonPipe,
    FiltersComponent,
    FiltreObjetsPipe,
    AsyncPipe,
    RouterModule,
    CocktailFavoriStarsComponent
   ],
  templateUrl: './liste-cocktails.component.html',
  styleUrl: './liste-cocktails.component.scss'
})
export class ListeCocktailsComponent {
  cocktails$: Observable<Cocktail[]>;
  labelSearchComponent: string = 'Cocktail name'
  filterByName: string = ''

  
  constructor(public cocktailService: CocktailsService){}

  ngOnInit(){
    this.cocktails$ = this.cocktailService.getAll();
  }

  /**
   * On récupère la valeur du bandeau générique de filtres
   * On peut ainsi maintenir à jour un filtre actif sur les cocktails et qui pourrais être utilisé 
   * par d'autres composants
   * @param value 
   */
  onFilterByNameValueChange(value: any){
    this.filterByName = value;
  }

  /**
   * Propage la modification du statut de favorie d'un cocktail
   * @param cocktailId 
   */
  toggleFavori(cocktailId: string): void {
    this.cocktailService.toggleFavorie(cocktailId);
  }
}


