export class Cocktail {
    constructor(
        public id: string,
        public name?: string,
        public isAlcoholic = true,
        public imageUrl?: string,
        public instructions?: string,
        public ingredients = []
    ){}
}