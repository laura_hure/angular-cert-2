import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtreObjets',
  standalone: true
})
export class FiltreObjetsPipe implements PipeTransform {

  /**
   * Permet de filtrer une liste d'objets en fonction de la clé et valeurs passé en paramètre
   * @param liste Liste d'objets à filtrer
   * @param value valeur pour filtrer, forcément un string
   * @param key Clé sur laquelle filtrer, correspond au nom d'attribut d'un objet
   * @returns 
   */
  transform(objets: any[], value: string, key: string): any[] {
    if(value === '' || key === '')return objets;
    return objets ? objets.filter(obj => String(obj[key])?.toLocaleLowerCase().includes(value.toLowerCase())) : [];
  }

}
