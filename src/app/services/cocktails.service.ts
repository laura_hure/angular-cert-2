import { HttpClient } from '@angular/common/http';
import { Inject, Injectable, InjectionToken, Signal, signal, WritableSignal} from '@angular/core';
import { map, Observable, reduce } from 'rxjs';
import { Cocktail } from '../models/cocktail.model';


export const BROWSER_STORAGE = new InjectionToken<Storage>('Browser Storage', {
  providedIn: 'root',
  factory: () => localStorage
});

@Injectable({
  providedIn: 'root'
})
export class CocktailsService {

  constructor(private http: HttpClient, @Inject(BROWSER_STORAGE) public storage: Storage) {  }

  /**
   * Retourne tous les cocktails
   * @returns 
   */
  getAll() : Observable<Cocktail[]>{
    return this.http.get<Cocktail[]>('cockails');
  }

  /**
   * Retourne un cocktail en fonction de son identifiant
   * @param id 
   * @returns 
   */
  getById(id: string): Observable<Cocktail>{
    return this.http.get<Cocktail>(`cockails/${id}`)
  }

  /**
   *  Modifie la valeur de favorie d'un cocktail
   * @param id identifiant du cocktail
   * @param isFavorie nouvelle valeur de la données favorie
   */
  toggleFavorie(id: string): void{
    const newValue: boolean = !Boolean(this.getCocktailFavById(id))
    this.setStorage(id,newValue.toString())
  }

  /**
   * Indique si un cocktail est favorie ou non
   * @param id 
   * @returns 
   */
  getCocktailFavById(id: string): boolean{
    return this.getStorage(id)?.localeCompare('true') === 0
  }

  /**
   * Permet de récupérer des données dans le browser
   * @param key 
   * @returns 
   */
  private getStorage(key: string) : string | null{
    return this.storage.getItem(key)
  }

  /**
   * Permet de stocker des données dans le browser
   * @param key 
   * @param value 
   */
  private setStorage(key: string, value: string) {
    this.storage.setItem(key, value)
  }
}
